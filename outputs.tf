output "vm-ip" {
  value = module.ec2.vm-ip
}

output "rds-endpoint" {
  value = module.rds.rds-endpoint
}

output "sqs-endpoint" {
  value = module.sqs.sqs-endpoint
}
