variable "key_name" {
  default     = ""
  type        = string
  description = "Key pair name for instance"
}

variable "subnet_id" {
  default     = ""
  type        = string
  description = "Subnet ID for instance"
}

variable "sg_id" {
  default     = ""
  type        = string
  description = "Security Group ID for instance"
}

variable "instance_type" {
  default     = "t2.micro"
  type        = string
  description = "Instance Type for instance"
}

variable "ami" {
  default     = ""
  type        = string
  description = "AMI for instance"
}

variable "instance_name" {
  default     = ""
  type        = string
  description = "Instance Name for instance"
}

variable "subnet_ids" {
  default     = [""]
  type        = list(string)
  description = "Subnets IDs for RDS"
}

variable "allocated_storage" {
  default     = ""
  type        = string
  description = "Storage size for RDS"
}

variable "db_name" {
  default     = ""
  type        = string
  description = "Name for RDS Instance"
}

variable "sg_id_rds" {
  default     = ""
  type        = string
  description = "Security Group ID for RDS"
}

variable "engine" {
  default     = "postgres"
  type        = string
  description = "Engine for RDS"
}

variable "instance_class" {
  default     = "db.t3.micro"
  type        = string
  description = "Class for RDS"
}

variable "username" {
  default     = ""
  type        = string
  description = "Username for RDS"
}

variable "password" {
  default     = ""
  type        = string
  description = "Password for RDS"
}

variable "name" {
  default     = ""
  type        = string
  description = "Name for SQS queue"
}

variable "delay_seconds" {
  default     = ""
  type        = string
  description = "Delay Seconds for RDS SQS queue"
}

variable "max_message_size" {
  default     = ""
  type        = string
  description = "Max Message Size for SQS queue"
}

variable "message_retention_seconds" {
  default     = ""
  type        = string
  description = "Message Retention Seconds for SQS queue"
}

variable "receive_wait_time_seconds" {
  default     = ""
  type        = string
  description = "Receive Wait Time Seconds for SQS queue"
}