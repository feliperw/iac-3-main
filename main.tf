terraform {

  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.40.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "ec2" {
  source = "git::https://gitlab.com/feliperw/iac-3-ec2.git"

  key_name      = var.key_name
  subnet_id     = var.subnet_id
  sg_id         = var.sg_id
  instance_type = var.instance_type
  ami           = var.ami
  instance_name = var.instance_name

}

module "rds" {
  source = "git::https://gitlab.com/feliperw/iac-3-rds.git"

  subnet_ids        = var.subnet_ids
  allocated_storage = var.allocated_storage
  db_name           = var.db_name
  sg_id             = var.sg_id_rds
  engine            = var.engine
  instance_class    = var.instance_class
  username          = var.username
  password          = var.password

}

module "sqs" {
  source = "git::https://gitlab.com/feliperw/iac-3-sqs.git"

  name                      = var.name
  delay_seconds             = var.delay_seconds
  max_message_size          = var.max_message_size
  message_retention_seconds = var.message_retention_seconds
  receive_wait_time_seconds = var.receive_wait_time_seconds

}